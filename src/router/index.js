import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '../pages/HomePage'
import UsersPage from '../pages/UsersPage'
import UserPage from '../pages/UserPage'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'HomePage',
            component: HomePage
        },
        {
            path: '/users',
            name: 'UsersPage',
            component: UsersPage
        },
        {
            path: '/users/:id',
            name: 'UserPage',
            component: UserPage
        }
    ]
})
